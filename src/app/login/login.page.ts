import { Component, OnInit, AfterContentInit, AfterViewInit,OnDestroy} from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IonicModule,MenuController,ToastController,LoadingController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { DataServiceService } from '../service/data-service.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  Phone:any;
  user:any;
  isdisable:boolean;

  constructor(private service: DataServiceService,private router: Router,private toastCtrl: ToastController,public loadingCtrl: LoadingController,public menuCtrl: MenuController,private storage: Storage)
   { 
    this.storage.clear();
   }

  ngOnInit() {
  }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  async validatedriver(form: NgForm){   
    // console.log(form)
    if (form.invalid) {
      // alert('invalid')
      return;
    }
    let logindata={"data":
                    {"mobile_no":this.Phone}
                  }

    console.log(JSON.stringify(logindata));
    const loading = await this.loadingCtrl.create({
      message: ' please wait. . .',
    });
    loading.present();

    console.log("login:",'login');
    this.service.login(logindata,'login').then((result) => {
      loading.dismiss();
      console.log(result);
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      if(resArray[0]=="true"){
        // loading.dismiss();
        let driverdetails=resArray[2];

        this.storage.set('driver_id',driverdetails._id).then((driver_id)=>{
        });
        console.log("THIS DRIVERDETAILS.ID TRYING TO SET AT STORAGE :::::::::::::>>",driverdetails._id);
        this.service.loggedInPhone=driverdetails.user_mobile;
        this.service.driverid=driverdetails._id;
        this.service.loggedIndrivername=driverdetails.username;
        this.service.drivertype=driverdetails.user_type;
        console.log(this.service.loggedInPhone);
        console.log("THIS IS DRIVER ID ===> ",this.service.driverid);
        console.log(this.service.loggedIndrivername);
        console.log(this.service.drivertype);
        //this.router.navigateByUrl('/verification');
        this.router.navigate(['/otpverification', {otp:driverdetails.otp}]);
      }  
    },(err) =>{
      loading.dismiss;
      console.log("sign up eror",JSON.stringify(err));
    });   
  }


  async validnumber(event){
    console.log(event.target.value.length);
    if(event.target.value.length != 10){
      if(event.target.value.length>10){
        const toast =  await this.toastCtrl.create({
          message:"Please enter 10 digit Phone no.",
          duration: 1000
        });
        toast.present();
      }

      this.isdisable=true;
    }else{
      this.isdisable=false;
    }
  }

  // validateDriver(){
  //   this.router.navigateByUrl('/otpverification');
  //   // this.service.getDriver().then((res)=>{
  //   //   //console.log(res);
  //   //   this.user = res;
  //   //  // console.log(this.user.length);
  //   //   for(let i=0; i<this.user.length; i++){
  //   //     var u=this.user[i];
  //   //     //console.log(u.phone);
  //   //     //console.log(this.Phone);
  //   //       if(u.phone==this.Phone){
  //   //         ///console.log("login successfull");
  //   //         this.router.navigateByUrl('/otpverification');
  //   //         break;
  //   //       }else{
  //   //         //console.log("login unsuccessfull");
  //   //       }
  //   //   }
  //   // });
  // }
}
