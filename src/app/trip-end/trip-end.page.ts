import { Component, OnInit } from '@angular/core';
import { FareModalPage } from '../fare-modal/fare-modal.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-trip-end',
  templateUrl: './trip-end.page.html',
  styleUrls: ['./trip-end.page.scss'],
})
export class TripEndPage implements OnInit {

  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
  }

  async gotomodal(){
    const modal = await this.modalCtrl.create({
      component: FareModalPage
    });
    return await modal.present();

  }

}
