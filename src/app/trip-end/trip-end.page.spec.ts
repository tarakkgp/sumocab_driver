import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TripEndPage } from './trip-end.page';

describe('TripEndPage', () => {
  let component: TripEndPage;
  let fixture: ComponentFixture<TripEndPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TripEndPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TripEndPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
