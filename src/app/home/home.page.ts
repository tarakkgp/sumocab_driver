import { Component } from '@angular/core';
import { IonicModule,MenuController,ToastController,LoadingController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { DataServiceService } from '../service/data-service.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  checked:boolean=false;
  viewType:any;
  driverId:string;
  // driver_id:any;
  storage_driver_id:any;
  total_trips:string;
  total_payout:string;

  constructor(
    private service: DataServiceService,
    private router: Router,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public menuCtrl: MenuController,
    private storage: Storage
  )
  {}

    menutoggle(){
      this.menuCtrl.toggle();
    }

    onlineOfflineToggle() {
      console.log('checked ' + this.checked);
    }

    ngOnInit() {
      this.storage.get('driver_id').then((driver_id) => {
      this.storage_driver_id=driver_id;
      console.log("STORAGE DRIVER ID ::::::::>>",this.storage_driver_id); 
      this.driverSummery();  
    });

      
    }



    async driverSummery() {
      let driverSum={"data":{
        "driver_id": this.storage_driver_id
       }}
       console.log(driverSum);
      this.service.driverTripSummery(driverSum,'driver_offline_summery').then((result) => {
        //console.log("RESULT :::::::::::: >>",result); 
        const resArray = Object.keys(result).map(i => result[i]);
        //console.log(resArray[1]);
        this.total_trips=resArray[1];
        this.total_payout=resArray[2];
      },
      (err) =>{
        console.log("driver summery error",JSON.stringify(err));
      }); 
    }
  

}
