import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { Routes,RouterModule } from '@angular/router';
// import { HomeRoutingModule } from './home.router.module';
import { LoginPage } from '../login/login.page';
import { HomePage } from './home.page';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // HomeRoutingModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      },
    ])
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [HomePage]
})
export class HomePageModule {}
// export class HomePageModule {}

// const routes: Routes = [
//   {
//     path: 'tabs',
//     component: HomePage,
//     children: [
//       {
//         path: 'tab1',
//         loadChildren: './home/home.module#HomePageModule'
//       },
//       {
//         path: 'tab2',
//         loadChildren: './signup/signup.module#SignupPageModule'
//       }
//     ]
//   },
//   {
//     path: '',
//     redirectTo: 'tabs/tab1',
//     pathMatch: 'full'
//   }
// ];

// @NgModule({
//   imports: [
//     CommonModule,
//     FormsModule,
//     IonicModule,
//     RouterModule.forChild(routes)
//   ],
//   declarations: [HomePage]
// })
