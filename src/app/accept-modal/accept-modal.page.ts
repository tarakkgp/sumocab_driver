import { Component, OnInit } from '@angular/core';
import { RiderInfoPage } from '../rider-info/rider-info.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-accept-modal',
  templateUrl: './accept-modal.page.html',
  styleUrls: ['./accept-modal.page.scss'],
})
export class AcceptModalPage implements OnInit {

  constructor(public modalCtrl: ModalController) { }

  async gotomodal(){
    const modal = await this.modalCtrl.create({
      component: RiderInfoPage
    });
    return await modal.present();

  }

  ngOnInit() {
  }

}
