import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptModalPage } from './accept-modal.page';

describe('AcceptModalPage', () => {
  let component: AcceptModalPage;
  let fixture: ComponentFixture<AcceptModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
