import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

let apiUrl = "http://192.168.0.124:3000/api/";


@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
  
  [x: string]: any;
  drivertype:any;
  driverid:any;
  loggedIndriverername:any;
  loggedInPhone:any;

  user = "http://localhost:3000/driver";
  constructor(public http: HttpClient) { }

  
  saveDriver(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.user, data).subscribe(res => {
      resolve(res);
      console.log(res);
      }, (err) => {
      reject(err);
      console.log(err);
      });
    });
  }

  getDriver(){
    return new Promise((resolve, reject) => {
      this.http.get(this.user).subscribe(res => {
      resolve(res);
      //console.log(res);
      }, (err) => {
      reject(err);
      console.log(err);
      });
    });  
  }


  newRegister(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type,credentials).subscribe(res =>
        {
          resolve(res);
          //console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }

  login(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type,credentials).subscribe(res =>
        {
          resolve(res);
          //console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }

  otpverfiyed(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type,credentials).subscribe(res =>
        {
          resolve(res);
          //console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }

  driverTripSummery(credentials, type) {
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl + type,credentials).subscribe(res =>
        {
          resolve(res);
          console.log(res);
        },
        (err) =>
        {
          reject(err);
          console.log(err);
        }
      );
    });
  }

}


