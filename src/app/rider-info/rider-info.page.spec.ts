import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiderInfoPage } from './rider-info.page';

describe('RiderInfoPage', () => {
  let component: RiderInfoPage;
  let fixture: ComponentFixture<RiderInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiderInfoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiderInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
