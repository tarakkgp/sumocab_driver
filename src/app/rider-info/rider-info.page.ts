import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { ModalController} from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-rider-info',
  templateUrl: './rider-info.page.html',
  styleUrls: ['./rider-info.page.scss'],
})
export class RiderInfoPage implements OnInit {

  constructor(public modalCtrl: ModalController,public ref:ChangeDetectorRef,private router: Router) { }

  ngOnInit() {
  }

  closeModal()
  {
    this.modalCtrl.dismiss();
    this.router.navigateByUrl('/choosecab');
  }

}
