import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: '',
   redirectTo: 'login',
    pathMatch: 'full' 
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'otpverification', loadChildren: './otpverification/otpverification.module#OtpverificationPageModule' },
  { path: 'accept-modal', loadChildren: './accept-modal/accept-modal.module#AcceptModalPageModule' },
  { path: 'location', loadChildren: './location/location.module#LocationPageModule' },
  { path: 'trip-end', loadChildren: './trip-end/trip-end.module#TripEndPageModule' },
  { path: 'fare-modal', loadChildren: './fare-modal/fare-modal.module#FareModalPageModule' },

  { path: 'rider-info', loadChildren: './rider-info/rider-info.module#RiderInfoPageModule' },

 // { path: 'location', loadChildren: './location/location.module#LocationPageModule' },
  { path: 'trip-end', loadChildren: './trip-end/trip-end.module#TripEndPageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'mytrip', loadChildren: './mytrip/mytrip.module#MytripPageModule' },
  { path: 'help', loadChildren: './help/help.module#HelpPageModule' },
  { path: 'document', loadChildren: './document/document.module#DocumentPageModule' },
  { path: 'terms-condition', loadChildren: './terms-condition/terms-condition.module#TermsConditionPageModule' },  { path: 'myprofile', loadChildren: './myprofile/myprofile.module#MyprofilePageModule' },
  { path: 'wallet', loadChildren: './wallet/wallet.module#WalletPageModule' },








  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
