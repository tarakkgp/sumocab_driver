import { Component, OnInit, AfterContentInit, AfterViewInit,OnDestroy} from '@angular/core';
import { IonicModule,MenuController,LoadingController,ToastController } from '@ionic/angular';
import { ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import { DataServiceService } from '../service/data-service.service';
import { Storage } from '@ionic/storage';



@Component({
  selector: 'app-otpverification',
  templateUrl: './otpverification.page.html',
  styleUrls: ['./otpverification.page.scss'],
})
export class OtpverificationPage implements OnInit {
  userotp:any;
  driver_id:any;

  constructor(public menuCtrl: MenuController,
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private service: DataServiceService,
    private router: Router,
    private storage: Storage

    ) {
    this.userotp = this.route.snapshot.paramMap.get('otp');
    console.log(this.userotp);
   }

  ngOnInit() {
  
    this.storage.get('driver_id').then((driver_id) => {
    this.driver_id=driver_id;

    console.log("STORAGE DRIVER ID ::::::::>>",this.driver_id); 
    });


  }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }


  async otpverification(){
    let userData = {
      "data": {
        "otp": this.userotp,
        "user_id":this.service.driverid
      }
    }
    console.log(JSON.stringify(userData));
    const loading = await this.loadingCtrl.create({
      message: ' please wait. . .',
    });
    loading.present();
    this.service.otpverfiyed(userData,'otp_verification').then((result) => {
      console.log(result);
      const resArray = Object.keys(result).map(i => result[i]);
      console.log(resArray);
      loading.dismiss();
      if(resArray[0]=="true"){
        this.presentToast(resArray[1]);
        this.router.navigateByUrl('/home');
      }
    }, (err) =>{
      loading.dismiss;
      console.log("sign up eror",JSON.stringify(err));
    });   
  }

  async resendotpverification(){
    let userData = {
      "data": {
        "user_id":this.service.driverid
      }
    }
    console.log(JSON.stringify(userData));
    const loading = await this.loadingCtrl.create({
      message: ' please wait. . .',
    });
    loading.present();
    this.service.otpverfiyed(userData,'resend_otp').then((result) => {
      console.log(result);
      const resArray = Object.keys(result).map(i => result[i]);
      loading.dismiss();
      if(resArray[0]=="true"){
       this.userotp=resArray[1];
       console.log(this.userotp);
      }
    }, (err) =>{
      loading.dismiss();
      console.log("sign up eror",JSON.stringify(err));
    }); 
  }



}
