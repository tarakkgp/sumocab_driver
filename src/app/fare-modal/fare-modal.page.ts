import { Component, OnInit } from '@angular/core';
import { ModalController} from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fare-modal',
  templateUrl: './fare-modal.page.html',
  styleUrls: ['./fare-modal.page.scss'],
})
export class FareModalPage implements OnInit {

  constructor(public modalCtrl: ModalController,
    private router: Router) { }

  ngOnInit() {
  }

  closeModal()
  {
    this.modalCtrl.dismiss();
    this.router.navigateByUrl('/home');
  }

}
