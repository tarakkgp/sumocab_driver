import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../service/data-service.service';
import { NgForm } from '@angular/forms';
import { IonicModule,MenuController,ToastController,LoadingController } from '@ionic/angular'; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  // driver={
  //   id: '',
  //   name: '',
  //   phone: '',
  //   dl_no: '',
  //   dl_exp: '',
  //   dob: '',
  //   email: '',
  //   present_address: '',
  //   permanent_address: '',
  // };


    id: any;
    name: any;
    phone: any;
    dl_no: any;
    dl_exp: any;
    dob: any;
    email: any;
    present_address: any;
    permanent_address: any;
  
    isdisable:boolean;
  constructor(private service: DataServiceService,
    private toastCtrl: ToastController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private router: Router,) { }

  ngOnInit() {
  }

  // saveDriver() {
  //   //console.log(this.users);
  //   this.service.saveDriver(this.driver).then((result) => {
  //     console.log(result); 
  //     }, (err) => { 
  //     console.log(err);
  //     }); 
      
  // }

  
  // signupDriver(form: NgForm){   
  //   console.log(form)
  //   if (form.invalid) {
  //     // alert('invalid')
  //     return;
  //   }
  //   this.service.saveUser(this.driver).then((result) => {
  //     let userData = {
  //       "data": {
  //         "mobile_no": this.phone,
  //         "name":this.name
  //       }
  //     }
      
      
  //     console.log(result); 
  //   }, (err) => { 
  //     console.log(err);
  //   }); 
  // }

  async presentToast(msg) {
    let toast =  await this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }


  async signupDriver(form: NgForm){   
    console.log(form)
    if (form.invalid) {
      // alert('invalid')
      return;
    }else{
      let driverData = {
        "data": {
          // "id": this.id,
          "user_type" : "driver",
          "mobile_no": this.phone,
          "name": this.name,
          "dl_no": this.dl_no,
          "dl_expiry": this.dl_exp,
          "email": this.email,
          "dob": this.dob,
          "present_address": this.present_address,
          "permanent_address": this.permanent_address
        }
      }
      console.log(JSON.stringify(driverData));
      const loading = await this.loadingCtrl.create({
        message: ' please wait. . .',
      });
      loading.present();
      this.service.newRegister(driverData,'registration').then((result) => {
        console.log(result);
        loading.dismiss();
        const resArray = Object.keys(result).map(i => result[i]);
        console.log(resArray);
        if(resArray[0]=="true"){
          this.presentToast(resArray[1]);
          loading.dismiss();
          let driverdetails=resArray[2];
          this.service.loggedInPhone=driverdetails.user_mobile;
          this.service.userid=driverdetails._id;
          this.service.loggedInUsername=driverdetails.username;
          this.service.usertype=driverdetails.user_type;
          console.log(this.service.loggedInPhone);
          console.log(this.service.userid);
          console.log(this.service.loggedInUsername);
          console.log(this.service.usertype);
          //this.router.navigateByUrl('/verification');
          this.router.navigate(['/otpverification', {otp:driverdetails.otp}]);
        
        // this.router.navigateByUrl('/otpverification'); 

        } 

        }, (err) =>{
        loading.dismiss;
        console.log("sign up eror",JSON.stringify(err));
      });   
    }
    // this.service.saveUser(this.users).then((result) => {
    //   console.log(result); 
    // }, (err) => { 
    //   console.log(err);
    // }); 
  }

  async validnumber(event){
    console.log(event.target.value.length);
    if(event.target.value.length != 10){
      if(event.target.value.length>10){
        const toast =  await this.toastCtrl.create({
          message:"Please enter 10 digit Phone no.",
          duration: 1000
        });
        toast.present();
      }

      this.isdisable=true;
    }else{
      this.isdisable=false;
    }
  }



}
